#include "Maxigauge.h"

int Maxigauge_get_channel_status(long status, char* channel_status, char* error)
{
    switch(status)
    {
        case 0 :
            sprintf(channel_status, NULL);
            break;
        case 1 :
            sprintf(channel_status, "Underrange");//list of sensor status codes
            break;
        case 2 :
            sprintf(channel_status, "Overrange");
            break;
        case 3 :
            sprintf(channel_status, "Sensor error");
            break;
        case 4 :
            sprintf(channel_status, "Sensor off");
            break;
        case 5 :
            sprintf(channel_status, "No sensor");
            break;
        case 6 :
            sprintf(channel_status, NULL);
            sprintf(error, "Identification error!");
            return MAXI_FAILURE;
        default :
            sprintf(channel_status, NULL);
            sprintf(error, "UNKNOWN ERROR!");
            return MAXI_FAILURE;
    }
    return MAXI_SUCCESS;
}

int Maxigauge_configure(const int serial_port, char* error){

    struct termios options;
    memset(&options, 0, sizeof(options));  /* clear the new struct */
    cfsetispeed(&options, B9600); //baud rate
    cfsetospeed(&options, B9600);
    options.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON);
    options.c_oflag = 0;
    options.c_lflag &= ~(ECHO | ECHONL | IEXTEN );
    options.c_lflag |= ICANON | ISIG;

    options.c_cflag &= ~(CSIZE | PARENB);
    options.c_cflag |= CS8;
    options.c_cflag |= CLOCAL | CREAD;

    options.c_cc[VMIN]  = 0;
    options.c_cc[VTIME] = 0;
    options.c_cc[VEOL] = 0;
    options.c_cc[VEOL2] = 0;
    options.c_cc[VEOF] = 0x04;//options given here to send then expect a receive message


    if(tcsetattr(serial_port, TCSANOW, &options) != 0)
    {
        sprintf(error, "MAXIGAUGE: FAILURE TO CONFIGURE TERMIOS");
        return MAXI_FAILURE;
    }
    else
    {
        return MAXI_SUCCESS;
    }
}

int Maxigauge_connect(const char* port_id, char* error)
{
    memset(error, '\0', MAXI_MSG_SIZE* sizeof(char));
	int serial_port = open(port_id, O_RDWR | O_NOCTTY );
	if(serial_port == -1)                        // Error Checking 
	{
        sprintf(error, "MAXIGAUGE: ERROR OPENING DEVICE");
        return -1;
	}
	else
	{
        if(Maxigauge_configure(serial_port, error))
        {
            tcflush(serial_port,TCIOFLUSH);
            return -1;
        }
	}
	return serial_port;
}

int Maxigauge_disconnect(const int serial_port)
{
	return close(serial_port);
}

int Maxigauge_write(const int serial_port, char* msg, char* error)
{
	char ack[MAXI_MSG_SIZE];
	char command[MAXI_MSG_SIZE];
	memset(error, '\0', MAXI_MSG_SIZE* sizeof(char));
    memset(ack, '\0', MAXI_MSG_SIZE* sizeof(char));

	sprintf(command, "%s\r\n",msg);//write function will add the CF and LF automatically 

	int nbytes_written = write(serial_port, command, strlen(command)+1);
	if(nbytes_written != strlen(command)+1){sprintf(error, "BYTES WRITTEN MISMATCH!");return MAXI_FAILURE;} //error check to make sure the length of the written message is correct length, including the CF and LF
	
	int bytes_available = 0;
    long msec = 0, trigger = 5000; /* 5s to wait before timeout */
    clock_t before = clock();
    while(bytes_available<1)
    {
        clock_t difference = clock() - before;
        msec = difference * 1000 / CLOCKS_PER_SEC; //50-60 msec delay for communication
        ioctl(serial_port, FIONREAD, &bytes_available);
        if(msec > trigger)
        {
            sprintf(error, "MAXIGAUGE: READ TIMEOUT\n");
            return MAXI_FAILURE;
        }
    }

	read(serial_port, ack, sizeof(char)*MAXI_MSG_SIZE);
	if(ack[0] == 0x06) //if the expected reply is ENQ then ACK is written, this is required to receive the information from the command
	{
		write(serial_port, (char[]){0x05},1); // Send ENQ
		return MAXI_SUCCESS;
	}
	else if (ack[0] == 0x15)
	{
	    sprintf(error, "NEGATIVE ACKNOWLEDGEMENT ON WRITE!");
	    return MAXI_FAILURE;
	}
	else sprintf(error, "UNKNOWN ERROR!"); return MAXI_FAILURE;//if there is a problem, the standard reply with negative acknowledgement'
}

int Maxigauge_read(const int serial_port, char* reply, char* error)
{
	int retval = 0;
    fd_set readfds;
    struct timeval timeout;
    memset(error, '\0', MAXI_MSG_SIZE* sizeof(char));
    memset(reply,'\0',MAXI_MSG_SIZE* sizeof(char));

    /*int bytes_available = 0;
    long msec = 0, trigger = 5000; //5s to wait before timeout
    clock_t before = clock();
    while(bytes_available<1)
    {
        clock_t difference = clock() - before;
        msec = difference * 1000 / CLOCKS_PER_SEC; //50-60 msec delay for communication
        ioctl(serial_port, FIONREAD, &bytes_available);
        if(msec > trigger)
        {
            sprintf(error, "MAXIGAUGE: READ TIMEOUT\n");
            return MAXI_FAILURE;
        }
    }*/

	FD_ZERO(&readfds);
	FD_SET(serial_port, &readfds);

	timeout.tv_sec = 1; //1 second timeout
	timeout.tv_usec = 0; 

	retval = select(serial_port+1, &readfds, NULL, NULL, &timeout);

	if(retval == -1){
		sprintf(error, "Maxigauge select() errno: %d", errno);
	}
	else if(retval == 0)
	{
		sprintf(error, "MAXIGAUGE: READ TIMEOUT!");
        return MAXI_FAILURE;
	}

	read(serial_port, reply, MAXI_MSG_SIZE);
	if(reply[0] == 0x15)
	{
	    sprintf(error, "NEGATIVE ACKNOWLEDGEMENT ON READ!");
	    return MAXI_FAILURE;
	}
	return MAXI_SUCCESS;//any issue with reply then get negative acknowledgement
}

int Maxigauge_get_error_status(const int serial_port, char* error)
{
    int error_status[2];
    char error_msg[MAXI_MSG_SIZE];
    char reply[MAXI_MSG_SIZE];
    memset(error,'\0',MAXI_MSG_SIZE* sizeof(char));

    if(Maxigauge_write(serial_port, "ERR", error_msg)){sprintf(error, "ERROR WRITING FOR DEVICE CHECK: %s", error_msg); return MAXI_FAILURE;}//error status command

    if(Maxigauge_read(serial_port, reply, error_msg)){sprintf(error, "ERROR READING FOR DEVICE CHECK: %s", error_msg);return MAXI_FAILURE;}

    char lreply[MAXI_MSG_SIZE];
    unsigned int size = strlen(reply);
    snprintf(lreply, size,"%s",reply);//remove CR LF from name

    sscanf(lreply,"%d,%d",&error_status[0], &error_status[1]); // split error and sensor message

    char msg1[256], msg2[256]; //defines the length of the two messages
    switch(error_status[0])
    {
        case 0:
            sprintf(msg1, "No error");//list of error codes
            break;
        case 1:
            sprintf(msg1, "Watchdog has responded");
            break;
        case 2:
            sprintf(msg1, "Task fail error");
            break;
        case 4:
            sprintf(msg1, "IDCX idle error");
            break;
        case 8:
            sprintf(msg1, "Stack overflow error");
            break;
        case 16:
            sprintf(msg1, "EPROM error");
            break;
        case 32:
            sprintf(msg1, "RAM error");
            break;
        case 64:
            sprintf(msg1, "EEPROM error");
            break;
        case 128:
            sprintf(msg1, "Key error");
            break;
        case 4096:
            sprintf(msg1, "Syntax error");
            break;
        case 8192:
            sprintf(msg1, "Inadmissible parameter");
            break;
        case 16384:
            sprintf(msg1, "No hardware");
            break;
        case 32768:
            sprintf(msg1, "Fatal error");
            break;
        default :
            sprintf(msg1, "Unknown error");
            break;
    }
    switch(error_status[1])
    {
        case 0:
            sprintf(msg2, "No error");
            break;
        case 1:
            sprintf(msg2, "Sensor 1: Measurement error");
            break;
        case 2:
            sprintf(msg2, "Sensor 2: Measurement error");
            break;
        case 4:
            sprintf(msg2, "Sensor 3: Measurement error");
            break;
        case 8:
            sprintf(msg2, "Sensor 4: Measurement error");
            break;
        case 16:
            sprintf(msg2, "Sensor 5: Measurement error");
            break;
        case 32:
            sprintf(msg2, "Sensor 6: Measurement error");
            break;
        case 512:
            sprintf(msg2, "Sensor 1: Identification error");
            break;
        case 1024:
            sprintf(msg2, "Sensor 2: Identification error");
            break;
        case 2048:
            sprintf(msg2, "Sensor 3: Identification error");
            break;
        case 4096:
            sprintf(msg2, "Sensor 4: Identification error");
            break;
        case 8192:
            sprintf(msg2, "Sensor 5: Identification error");
            break;
        case 16384:
            sprintf(msg2, "Sensor 6: Identification error");
            break;
        default :
            sprintf(msg2, "Unknown error");
            break;
    }
    sprintf(error, "Maxigauge error status: %s, Sensor error status: %s",msg1, msg2);//prints the error status for both the gauge and the sensors

    return MAXI_SUCCESS;
}

int Maxigauge_identify(const int serial_port, char* reply, char* error)
{
    char error_msg[MAXI_MSG_SIZE];
    char lreply[MAXI_MSG_SIZE];
    memset(lreply, '\0', MAXI_MSG_SIZE* sizeof(char));

	if(Maxigauge_write(serial_port, "PNR", error_msg))//command for identify of the maxigauge programme
	{
	    char error_handle[MAXI_MSG_SIZE];
        if(Maxigauge_get_error_status(serial_port, error_handle)){sprintf(error,"%s",error_handle); return MAXI_FAILURE;}
	    sprintf(error, "%s\nDEVICE CHECK: %s", error_msg, error_handle);
	    return MAXI_FAILURE;
	}

	if(Maxigauge_read(serial_port, lreply, error_msg))
	{
        char error_handle[MAXI_MSG_SIZE];
        if(Maxigauge_get_error_status(serial_port, error_handle)){sprintf(error,"%s",error_handle); return MAXI_FAILURE;}
        sprintf(error, "%s\nDEVICE CHECK: %s", error_msg, error_handle);
        return MAXI_FAILURE;
	}

	unsigned int size = strlen(lreply);//length of the string to remove the CF and LF from any returned message
	snprintf(reply, size,"%s",lreply);//removes CF and LF

	return MAXI_SUCCESS;
}

int Maxigauge_measure_pressure(const int serial_port, double* pressure, int channel_num, char* channel_status, char* error)
{
	char lreply[MAXI_MSG_SIZE];
	char msg[MAXI_MSG_SIZE];
	char reply[MAXI_MSG_SIZE];
	char error_msg[MAXI_MSG_SIZE];
	long status_num = 0;
	memset(error,'\0',MAXI_MSG_SIZE* sizeof(char));
    memset(lreply, '\0', MAXI_MSG_SIZE* sizeof(char));
    memset(reply, '\0', MAXI_MSG_SIZE* sizeof(char));
    memset(msg, '\0', MAXI_MSG_SIZE* sizeof(char));

	if(channel_num > 6 || channel_num <= 0){sprintf(error, "CHANNEL NUMBER OUT OF RANGE!"); return MAXI_FAILURE;}//checks the input from the user
	sprintf(msg, "PR%i", channel_num);//command to request sensor status and pressure for given channel
    if(Maxigauge_write(serial_port, msg, error_msg))
    {
        char error_handle[MAXI_MSG_SIZE];
        if(Maxigauge_get_error_status(serial_port, error_handle)){sprintf(error,"%s",error_handle); return MAXI_FAILURE;}
        sprintf(error, "%s\nDEVICE CHECK: %s", error_msg, error_handle);
        return MAXI_FAILURE;
    }

    if(Maxigauge_read(serial_port, lreply, error_msg))
    {
        char error_handle[MAXI_MSG_SIZE];
        if(Maxigauge_get_error_status(serial_port, error_handle)){sprintf(error,"%s",error_handle); return MAXI_FAILURE;}
        sprintf(error, "%s\nDEVICE CHECK: %s", error_msg, error_handle);
        return MAXI_FAILURE;
    }

    unsigned int size = strlen(lreply);//length of the string to remove the CF and LF from any returned message
    snprintf(reply, size,"%s",lreply);//removes CF and LF

    char string_pressure[MAXI_MSG_SIZE];
    memset(string_pressure,'\0', MAXI_MSG_SIZE* sizeof(char));
    for (int length = 2; length < strlen(reply)+1; length++) {//ignore the status and comma, then strip message down to just the pressure
        strncat(string_pressure, &reply[length], 1);
    }
    *pressure = strtod(string_pressure, NULL);//pass the pressure to component of the array for given channel number
    //return format for PRx is x,x.xxxEsx, this splits this up to status and pressure, without carrying the comma

    status_num = strtol(&reply[0],NULL,10);

    if(Maxigauge_get_channel_status(status_num, channel_status, error_msg)){sprintf(error,"%s",error_msg); return MAXI_FAILURE;}

	return MAXI_SUCCESS;
}

int Maxigauge_measure_multi_channel(const int serial_port, double* multi_pressure, char* error)
{
	char lreply[MAXI_MSG_SIZE];
	char msg[MAXI_MSG_SIZE];
	char reply[MAXI_MSG_SIZE];
	char error_msg[MAXI_MSG_SIZE];
	memset(error,'\0',MAXI_MSG_SIZE* sizeof(char));

    for(int chan = 0; chan<6; ++chan) //to loop over all channels possible
    {
        memset(msg, '\0', MAXI_MSG_SIZE* sizeof(char));
        memset(lreply, '\0', MAXI_MSG_SIZE* sizeof(char));
        memset(reply, '\0', MAXI_MSG_SIZE* sizeof(char));
        sprintf(msg, "PR%i", chan+1);//command to request sensor status and pressure for given channel
        if(Maxigauge_write(serial_port, msg, error_msg))
        {
            char error_handle[MAXI_MSG_SIZE];
            if(Maxigauge_get_error_status(serial_port, error_handle)){sprintf(error,"%s",error_handle); return MAXI_FAILURE;}
            sprintf(error, "%s\nDEVICE CHECK: %s", error_msg, error_handle);
            return MAXI_FAILURE;
        }
        if(Maxigauge_read(serial_port, lreply, error_msg))
        {
            char error_handle[MAXI_MSG_SIZE];
            if(Maxigauge_get_error_status(serial_port, error_handle)){sprintf(error,"%s",error_handle); return MAXI_FAILURE;}
            sprintf(error, "%s\nDEVICE CHECK: %s", error_msg, error_handle);
            return MAXI_FAILURE;
        }
        unsigned int size = strlen(lreply);//length of the string to remove the CF and LF from any returned message
        snprintf(reply, size,"%s",lreply);//removes CF and LF

        char string_pressure[MAXI_MSG_SIZE];
        if(strncmp ("3", reply,1) > 0)//compare the status, if above 3 then either sensor not connected or its not working so set pressure to 0
        {
            memset(string_pressure,'\0', MAXI_MSG_SIZE* sizeof(char));
            for (int length = 2; length < strlen(reply)+1; length++) {//ignore the status and comma, then strip message down to just the pressure
                strncat(string_pressure, &reply[length], 1);
            }
            multi_pressure[chan] = strtod(string_pressure, NULL);//pass the pressure to component of the array for given channel number
            //return format for PRx is x,x.xxxEsx, this splits this up to status and pressure, without carrying the comma
        }
        else{multi_pressure[chan] = 0;}//in all other cases pressure is 0
    }

	return MAXI_SUCCESS;
}

int Maxigauge_get_channel_ID(const int serial_port, unsigned int channel_num, char (*sens_id)[MAXI_MSG_SIZE], char* error)//todo: check if the sens_id length definination does not need to be in the function
{
    char lreply[MAXI_MSG_SIZE];
    char sens_id_str[MAXI_MSG_SIZE];
    char error_msg[MAXI_MSG_SIZE];
    memset(lreply, '\0', MAXI_MSG_SIZE* sizeof(char));

	if(channel_num > 6 || channel_num <= 0){sprintf(error, "CHANNEL NUMBER OUT OF RANGE!");return MAXI_FAILURE;}//checks to see if input within acceptable range
    channel_num = channel_num -1;//function starts at 0 so for user, they can input the channel number

    if(Maxigauge_write(serial_port, "TID", error_msg))
    {
        char error_handle[MAXI_MSG_SIZE];
        if(Maxigauge_get_error_status(serial_port, error_handle)){sprintf(error,"%s",error_handle); return MAXI_FAILURE;}
        sprintf(error, "%s\nDEVICE CHECK: %s", error_msg, error_handle);
        return MAXI_FAILURE;
    }

    if(Maxigauge_read(serial_port, lreply, error_msg))
    {
        char error_handle[MAXI_MSG_SIZE];
        if(Maxigauge_get_error_status(serial_port, error_handle)){sprintf(error,"%s",error_handle); return MAXI_FAILURE;}
        sprintf(error, "%s\nDEVICE CHECK: %s", error_msg, error_handle);
        return MAXI_FAILURE;
    }

    unsigned int size = strlen(lreply);//length of the string to remove the CF and LF from any returned message
    snprintf(sens_id_str, size,"%s",lreply);//removes CF and LF

	int id_num = 0;
	char* sens_id_tok = strtok (sens_id_str,",");//loop to take only the sensor type for given channel
	while (sens_id_tok != NULL)
	{
		if(id_num == channel_num){break;}
		sens_id_tok = strtok (NULL, ",");
		id_num++;//todo: need to check how this works and add error handling here
	}
	strcpy(*sens_id,sens_id_tok);
	return MAXI_SUCCESS;
}

int Maxigauge_set_channel(const int serial_port, unsigned int channel_num, unsigned int state, char* error)
{
	char msg[MAXI_MSG_SIZE];
	char error_msg[MAXI_MSG_SIZE];
	char reply[MAXI_MSG_SIZE];
    memset(error, '\0', MAXI_MSG_SIZE* sizeof(char));

	if(channel_num > 6 || channel_num <= 0){sprintf(error, "CHANNEL NUMBER OUT OF RANGE!");return MAXI_FAILURE;}
	if(state > 1 || state < 0){sprintf(error, "CHANNEL STATE OUT OF RANGE!");return MAXI_FAILURE;}
    state = state==0 ? 1 : 2;//checks to make sure inputs are within range
	//the SEN function has three states: 0 = no change, 1 = off, 2 = on. Therefore, these lines make the user input 0=off and 1=on
	
	channel_num = channel_num -1;//function starts at 0 so for user, they can input the channel

	char gauge_state[] = ",0,0,0,0,0,0";//initilze 
	char sensor_state[] = "0";

	snprintf(sensor_state,sizeof(char)+1,"%d",state);
	memcpy(gauge_state+(2*channel_num)+1, sensor_state, sizeof(char));
	sprintf(msg, "SEN %s\r\n",gauge_state);//SEN function followed by string with CF and LF

    if(Maxigauge_write(serial_port, msg, error_msg))
    {
        char error_handle[MAXI_MSG_SIZE];
        if(Maxigauge_get_error_status(serial_port, error_handle)){sprintf(error,"%s",error_handle); return MAXI_FAILURE;}
        sprintf(error, "%s\nDEVICE CHECK: %s", error_msg, error_handle);
        return MAXI_FAILURE;
    }

    if(Maxigauge_read(serial_port, reply, error_msg)) //required as the gauge will reply with something and this needs to be read otherwise its read in the write function.
    {
        char error_handle[MAXI_MSG_SIZE];
        if(Maxigauge_get_error_status(serial_port, error_handle)){sprintf(error,"%s",error_handle); return MAXI_FAILURE;}
        sprintf(error, "%s\nDEVICE CHECK: %s", error_msg, error_handle);
        return MAXI_FAILURE;
    }
	return MAXI_SUCCESS;
}
