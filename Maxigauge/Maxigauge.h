#ifndef __Maxigauge__
#define __Maxigauge__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>  
#include <termios.h> 
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <time.h>

#include <sys/uio.h>
#include <sys/socket.h>
#define MAXI_MSG_SIZE 512
#define MAXI_FAILURE 1
#define MAXI_SUCCESS 0
#define MAXI_CONNECT_FAIL -1

int Maxigauge_connect(const char* port_id, char* error);
int Maxigauge_disconnect(const int serial_port);
int Maxigauge_identify(const int serial_port, char* reply, char* error);
int Maxigauge_measure_pressure(const int serial_port, double* pressure, int channel_num, char* channel_status, char* error);
int Maxigauge_get_channel_ID(const int serial_port, unsigned int channel_num, char (*sens_id)[MAXI_MSG_SIZE], char* error);
int Maxigauge_set_channel(const int serial_port, unsigned int channel_num, unsigned int state, char* error);
int Maxigauge_measure_multi_channel(const int serial_port, double* multi_pressure, char* error);

#endif
