#include <stdio.h>
#include <string.h>

#include "Maxigauge.h"

int main(void)
{
    char maxi_error[MAXI_MSG_SIZE];
    char maxi_ID[MAXI_MSG_SIZE];
    char maxi_channel_ID[MAXI_MSG_SIZE];
    char maxi_channel_status[MAXI_MSG_SIZE];

	int maxi_fd = Maxigauge_connect("/dev/maxigauge_02", maxi_error);
	if(maxi_fd == -1){printf("%s\n",maxi_error);return -1;}
	
	if(Maxigauge_identify(maxi_fd,maxi_ID, maxi_error)){printf("%s\n",maxi_error); return -1;}//sends back gauge programme id
	else{printf("Maxigauge: %s\n",maxi_ID);}

	if(Maxigauge_get_channel_ID(maxi_fd, 1, &maxi_channel_ID, maxi_error)){printf("%s\n", maxi_error);return -1;}//sends back type of sensors connected for a given channel
	else{printf("%s\n",maxi_channel_ID);}

//	if(Maxigauge_set_channel(maxi_fd,1,1,maxi_error)){printf("%s\n", maxi_error);return -1;}//for given channel turns sensor on or off. NOT ALL SENSORS CAN BE TURNED ON! If this occurs, programme will reply with negative acknoledgement

//	sleep(1); //allows the gauge to turn on!

	double pressure;
	int channel_num = 1;
	if(Maxigauge_measure_pressure(maxi_fd, &pressure, channel_num, maxi_channel_status, maxi_error)){{printf("%s\n", maxi_error);return -1;}}//gets the sensor status and pressure

	printf("Pressure CH%i: %.3e mbar\n",channel_num,pressure);
	if(strcmp(maxi_channel_status, "") > 0){printf("CH%d: %s\n",channel_num,maxi_channel_status);}

	double multi_pressure[6];
    if(Maxigauge_measure_multi_channel(maxi_fd, multi_pressure, maxi_error)){printf("%s\n", maxi_error);return -1;}
    for(int chan = 0; chan<6; chan++) {
        printf("%f\n", multi_pressure[chan]);
    }
	Maxigauge_disconnect(maxi_fd);
} 
